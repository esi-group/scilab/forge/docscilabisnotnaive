% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/


\section{Appendix}

In this section, we analyze the examples given in the introduction of this 
article. In the first section, we analyze how the real number $0.1$ is represented by a double precision
floating point number, which leads to a rounding error. In the second 
section, we analyze how the computation of $\sin(\pi)$ is performed.
In the final section, we make an experiment which shows that $\sin(2^{10i} \pi)$ can 
be arbitrary far from zero when we compute it with double precision floating point 
numbers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Why $0.1$ is rounded}

In this section, we present a brief explanation for the 
following Scilab session. 
It shows that the mathematical equality $0.1=1-0.9$ is not exact 
with binary floating point integers.
\lstset{language=scilabscript}
\begin{lstlisting}
-->format(25)
-->x1=0.1
 x1  =
    0.1000000000000000055511  
-->x2 = 1.0-0.9
 x2  =
    0.0999999999999999777955  
-->x1==x2
 ans  =
  F  
\end{lstlisting}

We see that the real decimal number 0.1 is displayed as $0.100000000000000005$.
In fact, only the 17 first digits after the decimal point are 
significant : the last digits are a consequence of the approximate conversion 
from the internal binary double number to the displayed decimal number.

In order to understand what happens, we must decompose the 
floating point number into its binary components. 
The IEEE double precision floating point numbers used by Scilab 
are associated with a radix (or basis) $\beta=2$, 
a precision $p=53$, a minimum exponent $e_{min}=-1023$
and a maximum exponent $e_{max}=1024$.
Any floating point number $x$ is represented as 
\begin{eqnarray}
fl(x) = M \cdot \beta^{e-p+1},
\end{eqnarray}
where 
\begin{itemize}
\item $e$ is an integer called the exponent, 
\item $M$ is an integer called the integral significant.
\end{itemize}
The exponent satisfies $e_{min}\leq e\leq e_{max}$ while the integral significant 
satisfies $|M| \leq \beta^p - 1$. 

Let us compute the exponent and the integral significant of the number $x=0.1$.
The exponent is easily computed by the formula 
\begin{eqnarray}
e = \lfloor \log_2(|x|) \rfloor,
\end{eqnarray}
where the $\log_2$ function is the base-2 logarithm function.
In the case where an underflow or an overflow occurs, the value of 
$e$ is restricted into the minimum and maximum exponents range.
The following session shows that the binary exponent associated 
with the floating point number 0.1 is -4.
\begin{lstlisting}
-->format(25)
-->x = 0.1
 x  =
    0.1000000000000000055511  
-->e = floor(log2(x))
 e  =
  - 4.  
\end{lstlisting}
We can now compute the integral significant associated with this 
number, as in the following session.
\lstset{language=scilabscript}
\begin{lstlisting}
-->M = x/2^(e-p+1)
 M  =
    7205759403792794.  
\end{lstlisting}
Therefore, we deduce that the integral significant is equal to the decimal 
integer $M=7205759403792794$.
This number can be represented in binary form as the 53 binary digit number 
\begin{eqnarray}
M = 11001100110011001100110011001100110011001100110011010.
\end{eqnarray}
We see that a pattern, made of pairs of 11 and 00 appears. 
Indeed, the real value 0.1 is approximated by the following infinite 
binary decomposition:
\begin{eqnarray}
0.1 = \left(\frac{1}{2^0} + \frac{1}{2^1} + \frac{0}{2^2} + \frac{0}{2^3} + \frac{1}{2^4} + \frac{1}{2^5} + \ldots \right) \cdot 2^{-4}.
\end{eqnarray}

We see that the decimal representation of $x=0.1$ is made of a finite number of digits while the 
binary floating point representation is made of an infinite sequence of digits.
But the double precision floating point format must represent this number 
with 53 bits only. 

Notice that, the first digit is not stored in the binary double format, since it is 
assumed that the number is \emph{normalized} (that is, the first digit is assumed 
to be one). Hence, the leading binary digit is \emph{implicit}.
This is why there is only 52 bits in the mantissa, while we use 53 bits for the precision $p$.
For the sake of simplicity, we do not consider denormalized numbers in this discussion. 

In order to analyze how the rounding works, we look more carefully to the 
integer $M$, as in the following experiments, where we change 
only the last decimal digit of $M$.
\lstset{language=scilabscript}
\begin{lstlisting}
-->7205759403792793 * 2^(-4-53+1)
 ans  =
    0.0999999999999999916733  
-->7205759403792794 * 2^(-4-53+1)
 ans  =
    0.1000000000000000055511  
\end{lstlisting}
We see that the exact number 0.1 is between two consecutive floating point 
numbers:
\begin{eqnarray}
7205759403792793 \cdot 2^{-4-53+1} < 0.1 < 7205759403792794 \cdot 2^{-4-53+1}.
\end{eqnarray}
There are four rounding modes in the IEEE floating point standard. 
The default rounding mode is \emph{round to nearest}, which rounds to the nearest floating 
point number. 
In case of a tie, the rounding is performed to the only one of these 
two consecutive floating point numbers whose integral significant is even.
In our case, the distance from the exact $x$ to the two floating point 
numbers is 
\begin{eqnarray}
|0.1 - 7205759403792793\cdot 2^{-4-53+1}| &=& 8.33\cdots 10^{-18},\\
|0.1 - 7205759403792794\cdot 2^{-4-53+1}| &=& 5.55\cdots 10^{-18}.
\end{eqnarray}
(The previous computation is performed with a symbolic computation system, not with 
Scilab). 
Therefore, the nearest is $7205759403792794\cdot 2^{-4-53+1}$, 
which leads to $fl(0.1)= 0.100000000000000005$.

On the other side, $x=0.9$ is also not representable 
as an exact binary floating point number (but 1.0 is exactly represented). 
The floating point binary representation of $x=0.9$ is associated with 
the exponent $e=-1$ and an integral significant between 8106479329266892 and 8106479329266893.
The integral significant which is nearest to $x=0.9$ is 8106479329266893, which is associated with 
the approximated decimal number $fl(0.9)\approx 0.90000000000000002$. 

Then, when we perform the subtraction "1.0-0.9", the decimal representation of the 
result is $fl(1.0)-fl(0.9)\approx 0.09999999999999997$, which is different from 
$fl(0.1)\approx 0.100000000000000005$.

We have shown that the mathematical equality $0.1=1-0.9$ is not exact 
with binary floating point integers. 
There are many other examples where this happens. 
In the next section, we consider the sine function with a particular 
input.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Why $sin(\pi)$ is rounded}

In this section, we present a brief explanation of the following 
Scilab 5.1 session, where the function sinus is applied to the 
number $\pi$.
\begin{lstlisting}
-->format(10)
 ans  =
    0.  
-->sin(%pi)
 ans  =
    1.225D-16  
\end{lstlisting}

This article is too short to make a complete presentation 
of the computation of elementary functions. The interested 
reader may consider the direct analysis of the Fdlibm library
as very instructive \cite{fdlibm}.
Muller presents in "Elementary Functions" \cite{261217}
a complete discussion on this subject.

In Scilab, the $\sin$ function is connected to a 
fortran source code (located in the \emph{sci\_f\_sin.f} file), where 
we find the following algorithm:
\begin{lstlisting}
do i = 0 , mn - 1
  y(i) = sin(x(i))
enddo
\end{lstlisting}
The \scivar{mn} variable contains the number of elements in the matrix,
which is stored as the raw array \scivar{x}. This implies that 
no additionnal algorithm is performed directly by Scilab and 
the $\sin$ function is computed by the mathematical library provided 
by the compiler, i.e. by gcc under Linux and by Intel's Visual Fortran under Windows.

Let us now analyze the algorithm which is performed by the mathematical library 
providing the $\sin$ function. 
In general, the main structure of these algorithms is the following:
\begin{itemize}
\item scale the input $x$ so that in lies in a restricted
interval, 
\item use a polynomial approximation of the local 
behavior of $\sin$ in the neighborhood of 0.
\end{itemize}

In the Fdlibm library for example, the scaling interval is 
$[-\pi/4,\pi/4]$. 
The polynomial approximation of the $\sin$ function has the general form
\begin{eqnarray}
sin(x) &\approx& x + a_3x^3 + \ldots + a_{2n+1} x^{2n+1}\\
&\approx & x + x^3 p(x^2)
\end{eqnarray}
In the Fdlibm library, 6 terms are used.

For the \scifun{atan} function, which is 
used to compute an approximated value of $\pi$, the process is the same.
This leads to a rounding error in the representation of $\pi$ which is 
computed by Scilab as \scivar{4*atan(1.0)}.
All these operations are guaranteed with some precision, when applied to a 
number in the scaled interval. For inputs outside the scaling interval, the accuracy
depends on the algorithm used for the scaling.

All in all, the sources of errors in the floating point computation of 
$\sin(\pi)$ are the following
\begin{itemize}
\item the error of representation of $\pi$,
\item the error in the scaling,
\item the error in the polynomial representation of the function $\sin$.
\end{itemize}

The error of representation of $\pi$ by a binary floating point 
number is somewhat hidden by the variable name \scivar{\%pi}. 
In fact, we should really say that \scivar{\%pi} is the best possible 
binary double precision floating point number representation of the mathematical 
$\pi$ number. 
Indeed, the exact representation of $\pi$ would require an infinite number 
of bits. 
This is not possible, which leads to rounding. 

In fact the exact number $\pi$ is between two consecutive floating point 
numbers:
\begin{eqnarray}
7074237752028440 \cdot 2^{1-53+1} < \pi < 7074237752028441 \cdot 2^{1-53+1}.
\end{eqnarray}
In our case, the distance from the exact $\pi$ to its two nearest floating point 
numberss is 
\begin{eqnarray}
|0.1 - 7074237752028440 \cdot 2^{1-53+1}| &=& 1.22 \cdots 10^{-16},\\
|0.1 - 7074237752028441 \cdot 2^{1-53+1}| &=& 3.21 \cdots 10^{-16}.
\end{eqnarray}
(The previous computation is performed with a symbolic computation system, not with 
Scilab). 
Therefore, the nearest is $7074237752028440 \cdot 2^{1-53+1}$.
With a symbolic computation system, we find:
\begin{eqnarray}
\sin(7074237752028440 \cdot 2^{1-53+1}) = 1.22464679914735317e-16.
\end{eqnarray}
We see that Scilab has produced a result which has the maximum possible number 
of significant digits. 

We see that the rounding of \scivar{\%pi} has a tremendous effect 
on the computed value of \scivar{sin(\%pi)}, which is clearly 
explained by the condition number of this particular computation. 

The condition number of the a smooth single variable function $f(x)$ is 
$c(x) = |xf'(x)/f(x)|$. 
For the sine function, this is $c(x)=|x\cos(x)/\sin(x)$. 
This function is large when $x$ is large or when $x$ is an integer 
multiple of $\pi$. 
In the following session, we compute the condition number of the sine 
function at the point \scivar{x=\%pi}. 
\begin{lstlisting}
-->abs(%pi*cos(%pi)/sin(%pi))
 ans  =
    2.565D+16  
\end{lstlisting}
With such a large condition number, any change in the last significant bit 
of \scivar{x=\%pi} changes the first significant bit of \scivar{sin(x)}. 
This explains why computing \scivar{sin(\%pi)} is numerically challenging. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{One more step}

In fact, it is possible to reduce the number of 
significant digits of the sine function to as low as 0 significant digits.
We mathematical have $\sin(2^n \pi) = 0$, but this can be very inaccurate with 
floating point numbers. In the following Scilab session, we compute $\sin(2^{10i} \pi)$
for $i=1$ to 5.
\begin{lstlisting}
-->sin(2.^(10*(1:5)).*%pi).'
 ans  =
  - 0.0000000000001254038322  
  - 0.0000000001284092832066  
  - 0.0000001314911060035225  
  - 0.0001346468921407542141  
  - 0.1374419777062635961151  
\end{lstlisting}

For $\sin(2^{50}\pi)$, the result is very far from being zero. This computation
may sound \emph{extreme}, but it must be noticed that it is inside the 
IEEE double precision range of values, since $2^{50} \approx 3.10^{15} \ll 10^{308}$.
If accurate computations of the $\sin$ function are required for large values of 
$x$ (which is rare in practice), the solution may be to use multiple precision 
floating point numbers, such as in the MPFR library \cite{MPFRWeb,Fousse:2007:MMP}, based on the 
Gnu Multiple Precision library \cite{GMPWeb}. 


