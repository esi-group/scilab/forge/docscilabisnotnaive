// This file is released into the public domain
// cleaner.sce
// ------------------------------------------------------
curdir = pwd();
cleaner_path = get_file_path('cleaner.sce');
chdir(cleaner_path);
// ------------------------------------------------------
for ext = ["*.toc" "*.idx" "*.ilg" "*.ind" "*.log" "*.blg" "*.aux" "*.out" "*.bbl"]
  fl = ls(ext);
  if fl <> [] then
    for fn = fl'
      mprintf("Deleting %s\n",fn)
      mdelete(fn);
    end
  end
end
// ------------------------------------------------------
chdir(curdir);
// ------------------------------------------------------
