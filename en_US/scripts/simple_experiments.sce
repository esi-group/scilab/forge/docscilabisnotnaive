// Copyright (C) 2009 - Michael Baudin


// Why 0.1 is rounded ?
format(25)
x1=0.1
x2 = 1.0-0.9
x1==x2

// Let us decompose x=0.1 in fraction + exponent
x=0.1
e = floor(log2(x))
p = 53
M = x/2^(e-p+1)
7205759403792793 * 2^(-4-53+1)
7205759403792794 * 2^(-4-53+1)
// Exact, infinite, binary representation
y = sum([1 1 0 0 1 1 0 0 1 1 0 0].*2.^(0:-1:-11)) * 2^-4
(1 * 2^0 + 1 * 2^-1 + 0 * 2^-2 + 0 * 2^-3 + 1 * 2^-4 + 1 * 2^-5 + 0 * 2^-6) * 2^-4

// Let us decompose x=0.9 in fraction + exponent
x=0.9
e = floor(log2(x))
p = 53
M = x/2^(e-p+1)
8106479329266893 * 2^(-1-53+1)
8106479329266892 * 2^(-1-53+1)

