// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// number_bary --
//   Returns the list of digits of the decomposition of 
//   k in base b, i.e. decompose k as 
//   k = d0 b^jmax + d1 b^{jmax-1} + ... + d{jmax+1} b^0.
//   The order is little endian order, i.e. the first 
//   digit is associated with b^jmax, and the last digit
//   is associated with b^0.
// Arguments
//   k : the integer to decompose
//   basis : the basis (default 2)
//   order : the order (default "littleendian")
// References
//   Monte-Carlo methods in Financial Engineering, Paul Glasserman
// Test : number_bary (4,2) = [1 0 0]
//   number_bary (4,2,"bigendian") = [0 0 1]
// Calling sequences
//   digits = number_bary ( k )
//   digits = number_bary ( k , basis )
//   digits = number_bary ( k , basis , order )
//
function digits = number_bary ( varargin )
  [lhs,rhs]=argn();
  if ( ( rhs <> 1 ) & ( rhs <> 2 ) & ( rhs <> 3 ) ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while %d to %d are expected."), "number_bary" , rhs , 1 , 3 );
    error(errmsg)
  end
  select rhs
  case 1 then
    k = varargin ( 1 )
    basis = 2
    order = "littleendian";
  case 2 then
    k = varargin ( 1 )
    basis = varargin ( 2 )
    order = "littleendian";
  case 3 then
    k = varargin ( 1 )
    basis = varargin ( 2 )
    order = varargin ( 3 )
  end

  if ( basis < 2 ) then
    errmsg = msprintf(gettext("%s: Unexpected value for basis: Expected an integer greater than 2 but got %d instead."), "number_bary" , basis );
    error(errmsg)
  end

  select order
  case "littleendian"
    if (k==0) then
      digits = zeros(1,1);
    else
      jmax = int(log(k)/log(basis));
      q = int(basis^jmax);
      for j=1:jmax+1
        aj = int(k/q);
        digits(1,j) = aj;
        k = k - q * aj;
        q = q/basis;
      end
    end
  case "bigendian"
    if k==0 then
      digits = zeros(1,1);
    else
      jmax = int(log(k)/log(basis));
      current = k
      j = 1;
      while ( current > 0 )
        digit = modulo ( current , basis )
        digits(1,j) =digit
        current = int ( current / basis )
        j = j + 1
      end
    end
  else
    error ( mprintf ( gettext ( "%s: Unknown order"  ), "number_bary" ) )
  end
endfunction


