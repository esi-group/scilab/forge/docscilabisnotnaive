// Copyright (C) 2010 - Michael Baudin

// An attempt at implementing the complex division as
// in C99

// Bibliography
// ISO/IEC 9899 - Programming languages - C
// http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf




function r = copysign(x,y)
  // Returns r with the magnitude of x and the sign of y.
  // If y is positive or negative zero, the sign of r is positive.
  [lhs,rhs]=argn()
  if ( rhs<>2 ) then
    errmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
      "copysign",rhs,1,1)
    error(errmsg)
  end
  if ( size(x) <> size(y) ) then
    errmsg = sprintf(gettext("%s: Size of x and y are different."),..
      "copysign")
    error(errmsg)
  end
  r = zeros(x)
  k = find(y<>0)
  r(k) = abs(x(k)).*sign(y(k))
  k = find(y==0)
  r(k) = abs(x(k))
endfunction

if ( %f ) then

x = [-5 -4 0 4 5]
y = [-1 0 1 2 3]
r = copysign(x,y)
expected = [5 4 0 4 5]

end

