// Copyright (C) 2010 - Michael Baudin

// An attempt at implementing the complex division as
// in C99

// Bibliography
// ISO/IEC 9899 - Programming languages - C
// http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf


function X = pow2 ( varargin )
  // Base 2 power and scale floating-point numbers
  // X = pow2(Y) returns 2^Y
  // X = pow2(F,E) returns F.* 2 .^ E. 
  // pow2(1-%eps/2,1024) produces %inf instead of realmax
  // This function corresponds to the ANSI C function ldexp() and the IEEE floating-point standard function scalbn().
  // http://www.mathworks.fr/help/techdoc/ref/pow2.html

  [lhs,rhs]=argn()
  if ( and( rhs<>[1 2] ) ) then
    errmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
      "pow2",rhs,1,2)
    error(errmsg)
  end
  if ( rhs==1 ) then
    Y = varargin(1)
    X = 2.^Y
  else
    F = varargin(1)
    E = varargin(2)
    X = F.* 2 .^ E
  end
endfunction

if ( %f ) then
F = [1/2 %pi/4 -3/4 1/2 1-%eps/2 1/2];
E = [1 2 2 -51 1024 -1021];
X = pow2(F,E)
realmax = number_properties("huge")
realmin = number_properties("tiny")
expected = [1 %pi -3 %eps  realmax realmin]
X == expected 
// T T T T F T

end

