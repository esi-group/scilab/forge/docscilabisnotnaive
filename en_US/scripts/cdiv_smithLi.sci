// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// An improved Smith method, from Li et al.
// Li,, Xiaoye S. and Demmel,, James W. and Bailey,, David H. and Henry,, Greg and Hida,, Yozo and Iskandar,, Jimmy and Kahan,, William and Kang,, Suh Y. and Kapur,, Anil and Martin,, Michael C. and Thompson,, Brandon J. and Tung,, Teresa and Yoo,, Daniel J.},
// Design, implementation and testing of extended and mixed precision BLAS},
// ACM Trans. Math. Softw., vol. 28, numb. 2, 2002
// Tested in the normal range.
// TODO : test in the scaled cases.

function q = cdiv_smithLi ( x,y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    AB = max(abs([a b]))
    CD = max(abs([c d]))
    // The choice of Li et al. for B:
    B = 2
    S = 1
    OV = number_properties("huge")
    UN = number_properties("tiny")
    Be = B/%eps^2
    // Scaling
    if ( AB > OV/16 ) then
      // Scale down a, b
      a = a/16
      b = b/16
      S = S*16
    end
    if ( CD > OV/16 ) then
      // Scale down c, d
      c = c/16
      d = d/16
      S = S/16
    end
    if ( AB < UN*B/%eps ) then
      // Scale up a, b
      a = a * Be
      b = b * Be
      S = S/Be
    end
    if ( CD < UN*B/%eps ) then
      // Scale up c, d
      c = c * Be
      d = d * Be
      // The paper has a mistake there
      S = S*Be
    end
    // Now a, b, c, d in [UN*B/%eps,OV/16]
    if ( abs(c) > abs(d) ) then
      r = d/c
      t = 1/(c+d*r)
      qre = (a+b*r)*t
      qim = (b-a*r)*t
    else
      r = c/d
      t = 1/(d+c*r)
      qre = (b+a*r)*t
      qim = (-a+b*r)*t
    end
    // Scale back
    qre = qre * S
    qim = qim * S
    q = complex(qre,qim)      
endfunction

