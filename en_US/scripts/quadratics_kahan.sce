// W. Kahan
// qdrtctst.m is a Matlab script that tests qdrtc.m
// and puts its results into a diary file qdrtctst.res
diary qdrtctst.res
format("e")
'Test roots with extremely tiny imaginary parts:'
A = 2017810*8264001469
B = 39213*229699315399
C = 45077*107932908389
Z = (B + sqrt(-1))/A
D = -1
Derr = dscrmt(A, B, C) - D 
Derr0 = (B*B - A*C) - D
[Z1, Z2] = qdrtc(A, B, C)
[z1, z2] = qdrtc0(A,B,C)
qdrtcerr = Z1-Z
qdrtc0err = z1-Z
'- - - - - - - - - - - - - - - - - - - - - - - - - -'
'Produce test results for Table 2:'
B = 94906267 ; 
A = B - 1.375 ; 
C = B + 1.375 ;
B = [B; B+0.375] ; 
A = [A; A+0.75] ; 
C = [C; C] ;
ABC = [A, B, C]
D = [121/64; 1]
Derr = dscrmt(A,B,C) - D
Derr0 = (B.*B - A.*C) - D
[Z1, Z2] = qdrtc(A, B, C)
[z1, z2] = qdrtc0(A,B,C)
dZ = [-1, 1]*[Z1, Z2]
dz = [-1, 1]*[z1, z2]
'- - - - - - - - - - - - - - - - - - - - - - - - - -'
// Time qdrtc and qdrtc0 on large random arrays:
A = rand(100) - 0.5 ;
B = rand(100) - 0.5 ;
C = rand(100) - 0.5 ;
' Running qdrtc on 10000 random coefficients'
T = clock ;
[Z1, Z2] = qdrtc(A, B, C) ;
T = etime(clock, T) ;
' Running qdrtc0 on 10000 random coefficients'
t = clock ;
[z1, z2] = qdrtc0(A, B, C) ;
t = etime(clock, t) ;
TIMEqdrtc_vs_qdrtc0 = [T, t]
'- - - - - - - - - - - - - - - - - - - - - - - - - -'
'Generate test coefficients from Fibonacci numbers F_n :'
F = ones(79,1) ; 
I2 = F ; F(1) = 0 ; 
I2(2) = -1 ;
for n = 3:79
  F(n) = F(n-1)+F(n-2) ;
  I2(n) = -I2(n-1) ;
end
// F(n+1) = F_n ; I2(n+1) = F(n)^2 - F(n+1)*F(n-1) = (-1)^n
// ... To check on the test, ...
// 'Display the first dozen Fibonacci numbers in a row:'
// F0_11 = [ [0:11]; F(1:12)']
// 'Check the first 20 values F(n)^2 - F(n+1)*F(n-1) - I2(n+1)'
// Derr = norm( F(2:21).*F(2:21) - F(3:22).*F(1:20) - I2(3:22) )
// I = sqrt( I2(3:79) ) ; // THIS FAILS BECAUSE OF A BUG IN MATLAB 6.5
J = sqrt(-1) ; //... This is an alternative way to get I .
I = ones(1,39) ; I = [I; I*J] ; I = I(:) ; I = I(1:77) ;
A = F(3:79) ; B = F(2:78) ; C = F(1:77) ; D = I2(3:79) ;
// Compute first six [cZ1, cZ2] = [Roots] - 5/8 crudely,
// and the rest closely:
c = 0.625 ; 
cZ1 = I ; 
cZ2 = I ; //... to allocate memory
j = [1:6]' ;
cZ1(j) = B(j) - c*A(j) ;
j = [7:77]' ;
cZ1(j) = -0.125*A(j-6) ;
cZ2 = (cZ1 - I)./A ;
cZ1 = (cZ1 + I)./A ;
// Enlarge and randomize the test coefficients:
M = 1/eps ; 
M = rand(77, 1)*(M-1) + M ;
M = max(1, floor(M./A)) ; //... Let's not let M == 0 .
A = M.*A ; 
B = M.*B ; 
C = M.*C ;
trueD = M.*M.*I2(3:79) ; //... True Discriminant
// Compute 77 roots with and without dscrmt :
' Running qdrtc on Fibonacci coefficients'
T = clock ;
[Z1, Z2] = qdrtc(A, B, C) ;
T = etime(clock, T) ;
' Running qdrtc0 on Fibonacci coefficients'
t = clock ;
[z1, z2] = qdrtc0(A, B, C) ;
t = etime(clock, t) ;
TIMEqdrtc_vs_qdrtc0 = [T, t]
// ' Display the first dozen zeros to test the test:'
// j = [1:12]' ;
// TrueZ = [cZ1(j), cZ2(j)] + c
// qdrtcZ = [Z1(j), Z2(j)]
// qdrtc0z = [z1(j), z2(j)]
// Compute no. of correct sig. bits in roots:
L2e = 1/log(2) ; eta = eps*eps ; //... to prevent .../0
S1 = max( abs(c + cZ1)', eta ) ;
S2 = max( abs(c + cZ2)', eta ) ;
Zbits = max([ abs((Z1-c)-cZ1)'./S1; abs((Z2-c)-cZ2)'./S2 ]) ;
Zbits = min( 54, -log(Zbits' + eta)*L2e ) ;
zbits = max([ abs((z1-c)-cZ1)'./S1; abs((z2-c)-cZ2)'./S2 ]) ;
zbits = min( 54, -log(zbits' + eta)*L2e ) ;
// Plot no. of correct sig. bits in roots:
N = [2:78]' ;
plot(N, Zbits, N, zbits)
xlabel(' n ')
ylabel(' Correct sig. bits ')
title(' Accuracy of Roots from Fibonacci Coefficients ')
pause
// Compute no. of correct sig. bits in discriminants:
D = dscrmt(A, B, C) ;
d = B.*B - A.*C ;
Dbits = min(54, -log(abs((D-trueD)./trueD) + eta)*L2e ) ;
dbits = min(54, -log(abs((d-trueD)./trueD) + eta)*L2e ) ;
plot(N, Dbits, N, dbits)
xlabel(' n ')
ylabel(' Correct sig. bits ')
title(' Accuracy of Discriminants from Fibonacci Coefficients ')
pause

