// Copyright (C) 2010 - Michael Baudin

//
// naive --
//   Returns the complex division c = a / b 
//
function r = cdiv_naive (x,y)
  a = real(x)
  b = imag(x)
  c = real(y)
  d = imag(y)
  den = c * c + d * d
  e = (a * c + b * d) / den
  f = (b * c - a * d) / den
  // Avoid using x + %i * y, which may create
  // %inf+%i*%inf, which actually multiplies 0 and %inf,
  // generating an unwanted %nan.
  r = complex(e,f)
endfunction

