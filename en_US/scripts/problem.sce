// Copyright (C) 2009 - Michael Baudin

Z1 = 0.5401559 + 5.997D-17 * %i
Z2 = 0.5401559 - 5.997D-17 * %i
Z1 < Z2
gsort([Z1 Z2],"g","i")

Z1 = 0.5401559 + 5.997D-17 * i
Z2 = 0.5401559 - 5.997D-17 * i
Z1 < Z2
sort([Z1 Z2])

n = 5;
Z1 = rand(n,1)+rand(n,1)*%i;
Z2 = rand(n,1)+rand(n,1)*%i;
for i = 1:n
  disp([Z1(i) Z2(i)])
end
nD = (abs(Z1) < abs(Z2)) ; 
if or(nD) then //... Sort real roots
  S = Z1(nD) ; 
  Z1(nD) = Z2(nD) ; 
  Z2(nD) = S ; 
end
for i = 1:n
  disp([Z1(i) Z2(i)])
end

