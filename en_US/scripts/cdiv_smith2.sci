// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// An improved Smith method, with ideas from Stewart
function r = cdiv_smith2 ( x,y )
    a = real(x)
    b = imag(x)
    c = real(y)
    d = imag(y)
    if ( abs(d) <= abs(c) ) then
        z = 1 / c
        den = c + d * (d * z)
        e = (a + prodminmax ( [ b d z ] ) ) / den
        f = (b - prodminmax ( [ a d z ] ) ) / den
    else
        z = 1 / d
        den = c * (c * z) + d
        e = ( prodminmax ( [ a c z ] ) + b) / den
        f = ( prodminmax ( [ b c z ] ) - a) / den
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    r = complex(e,f)
endfunction
// A stable floating point product of a vector.
// Based on Stewart's ideas
function p = prodminmax ( v )
    n = size(v,"*")
    v = matrix(v,n,1)
    for i = 1 : n - 1
        // Search the maximum absolute value
        [ma,kmax]=max(abs(v))
        // Search the minimum absolute value in other values
        o = [v(1:kmax-1);v(kmax+1:$)]
        [mi,komin]=min(abs(o))
        // komin is the index in o
        // Compute the index kmin in v.
        if ( komin<=kmax-1 ) then
            kmin = komin
        else
            kmin = komin+1
        end
        v(kmin) = v(kmax) * v(kmin)
        v(kmax) = []
    end
    p = v(1)
endfunction

// A stable floating point product of a vector.
// Based on Stewart's ideas
// Does not delete entries in the vector v.
// More clever, but longer: forces to generate the matrices (1:i).
function p = prodminmax2 ( v )
    n = size(v,"*")
    v = matrix(v,n,1)
    for i = n-1 : -1 : 1
        // Search the maximum absolute value in v(1:i+1)
        [w,k]=max(abs(v(1:i+1)))
        // Switch the max and v(i+1)
        v([i+1,k]) = v([k,i+1])
        // Search the minimum absolute value in v(1:i)
        [w,k]=min(abs(v(1:i)))
        // Put the product into the min
        v(k) = v(k) * v(i+1)
    end
    p = v(1)
endfunction

if ( %f ) then
    p = prodminmax ( [1 3 1 2] ) // 6
    p = prodminmax ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    p = prodminmax ( [-1 1 1] )   // -1
    p = prodminmax ( [-1 -1 1] )  //  1
    p = prodminmax ( [-1 -1 -1] ) // -1
    p = prodminmax ( [-1 2 3] )    // -6
    p = prodminmax ( [-1 -2 3] )   //  6
    p = prodminmax ( [-1 2 -3] )   //  6
    p = prodminmax ( [1 2 -3] )   //  -6
    p = prodminmax ( [1.e200 1.e200 1.e-100] ) //  1.+300
    for k = 1 : 10
        v=round(10*rand(10,1)-5);
        v(find(v==0))=[];
        p1 = prodminmax ( v );
        p2 = prod ( v );
        disp([k p1 p2])
    end
    //
    p = prodminmax2 ( [1 3 1 2] ) // 6
    p = prodminmax2 ( [-1 2 -3 4 1 -2 3 4] ) //  -576
    p = prodminmax2 ( [-1 1 1] )   // -1
    p = prodminmax2 ( [-1 -1 1] )  //  1
    p = prodminmax2 ( [-1 -1 -1] ) // -1
    p = prodminmax2 ( [-1 2 3] )    // -6
    p = prodminmax2 ( [-1 -2 3] )   //  6
    p = prodminmax2 ( [-1 2 -3] )   //  6
    p = prodminmax2 ( [1 2 -3] )   //  -6
    p = prodminmax2 ( [1.e200 1.e200 1.e-100] ) //  1.+300
    for k = 1 : 10
        v=round(10*rand(10,1)-5);
        v(find(v==0))=[];
        p1 = prodminmax2 ( v );
        p2 = prod ( v );
        disp([k p1 p2])
    end
end
if ( %f ) then
    //
    // Test perfs
    exec("benchfun.sci")
    n=5000;
    v=round(10*rand(n,1)-5);
    benchfun ( "prodminmax " , prodminmax , list(v) , 1 , 10 );
    benchfun ( "prodminmax2" , prodminmax2 , list(v) , 1 , 10 );
    benchfun ( "prod       " , prod , list(v) , 1 , 10 );
end

