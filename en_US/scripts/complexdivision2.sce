// Copyright (C) 2009 - 2010 - Michael Baudin

// Some attempts to improve Smith's algorithm

haveimproved = %t;

// cdiv_naive.sci : a naive method
exec("cdiv_naive.sci");

// cdiv_smith.sci : original Smith's method
exec("cdiv_smith.sci"); 

// An improved Smith method, with ideas from Stewart (prodminmax)
exec("cdiv_smith2.sci");

// An improved Smith method, with an exp-log product
exec("cdiv_smith3.sci");

if ( haveimproved ) then
  // An improved Smith method
  exec("cdiv_smith4.sci");
end

// A Scilab port of the ANSI/ISO C algorithm
exec("cdiv_ansiisoC.sci");

exec("assert_computedigits.sci");

ieee(2);

// A table of input/outputs
// [a b c d e f]
// where the division is (a+ib)/(c+id) = e+if
table = [
 1      , 2       , 3      , 4       , 11/25  , 2/25
 1      , 2       , 4      , 3       , 2/5    , 1/5
 1      , 1       , 1      , 1e307   , 1e-307 , -1e-307
 1.e307 , 1.e-307 , 1e204  , 1e-204  , 1e103  , -1.e-305
 1      , 1       , 1e-307 , 1e-307  , 1e307  , 0
 1      , 1       , 1e-308 , 1.e-308 , 1.e308 , 0
 %inf      , %inf       , %inf , %inf , %nan , %nan
 %inf      , 0       , %inf , %inf , %nan , %nan
 0      , %inf       , %inf , %inf , %nan , %nan
 %inf      , %inf       , %inf , 0 , %nan , %nan
 %inf      , %inf       , 0 , %inf , %nan , %nan
];

ntests = size(table,"r");
basis = 2;
for k = 1 : ntests
  a = table(k,1);
  b = table(k,2);
  c = table(k,3);
  d = table(k,4);
  e = table(k,5);
  f = table(k,6);
  expectedR = e+%i*f;
  // Scilab Internal
  r = (a + %i * b )/(c + %i * d );
  digitsSci = assert_computedigits(r,expectedR,basis);
  // Naive
  r = cdiv_naive ( a + %i * b , c + %i * d );
  digitsNai = assert_computedigits(r,expectedR,basis);
  // Smith #1
  r = cdiv_smith ( a + %i * b , c + %i * d );
  digitsSm1 = assert_computedigits(r,expectedR,basis);
  // Smith #2
  r = cdiv_smith2 ( a + %i * b , c + %i * d );
  digitsSm2 = assert_computedigits(r,expectedR,basis);
  // Smith #3
  r = cdiv_smith3 ( a + %i * b , c + %i * d );
  digitsSm3 = assert_computedigits(r,expectedR,basis);
  if ( haveimproved ) then
  // Smith #4
  r = cdiv_smith4 ( a + %i * b , c + %i * d );
  digitsSm4 = assert_computedigits(r,expectedR,basis);
  end
  // ANSI/ISO C
  r = cdiv_ansiisoC ( a + %i * b , c + %i * d );
  digitsASC = assert_computedigits(r,expectedR,basis);
  // Print
  if ( haveimproved ) then
  mprintf("Test #%2d/%d, Sc = %2d, Na = %2d, Sm = %2d, Sm2 = %2d, Sm3 = %2d, Sm4 = %2d, ISOC = %.2d\n", ..
    k,ntests,digitsSci, digitsNai, digitsSm1, digitsSm2, digitsSm3, digitsSm4, digitsASC)
  else
  mprintf("Test #%2d/%d, Sc = %2d, Na = %2d, Sm = %2d, Sm2 = %2d, Sm3 = %2d, ISOC = %.2d\n", ..
    k,ntests,digitsSci, digitsNai, digitsSm1, digitsSm2, digitsSm3, digitsASC)
  end
end

// Why Stewart fails on (1 + i ) / (1e-308 + i * 1.e-308) = 1.e308 (exact result)?
// http://www.wolframalpha.com/input/?i=(1+%2B+i+)+/+(10^-308+%2B+i+*+10^-308)
// cdiv_smith2 ( 1 + %i,1e-308 + %i * 1.e-308 ) = 1.00D+308 + 5.55D+291 * i
// Here is why.
// The branch abs(d) <= abs(c) is chosen, where 
// z  = 1.00D+308   (exact)
// den  = 2.00D-308 (exact)
// e  = 1.00D+308   (exact)
// f  = 5.55D+291   (catastrophically wrong !)
// This cannot be avoided:
// f = (b - prodminmax ( [ a d z ] ) ) / den
// where b - prodminmax ( [ a d z ] ) = 1.110D-16  is the best result that 
// we can expect since :
// prodminmax ( [ a d z ] ) = 9.999999999999998890D-01
// The "stable" product has produced a relative error equal to %eps/2
// which is magnified by the division by den.
//
// To convince yourself :
// format("e",25)
// prodminmax ( [ 1 1.e-308 1.e308 ] ) = 9.999999999999998890D-01 (exact = 1)
// Better is exp-log :
// prodexplog ( [ 1 1.e-308 1.e308 ] ) = 1.000000000000000000D+00

// Smith #3 (based on prodexplog) can be innacurrate.

