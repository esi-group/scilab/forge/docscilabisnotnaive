// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// A complex division based on polar form.
// Can be very inaccurate:
// x=-1-%i
// y=-1+%i
// cdiv_polar(x,y) : - 1.837D-16 + i (Exact = %i)
// 
//
function r = cdiv_polar ( x , y )
  // Put x in to polar form x=xr*exp(xa*%i)
  xr = abs(x)
  xa = atan(imag(x),real(x))
  // Put y in to polar form y=yr*exp(ya*%i)
  yr = abs(y)
  ya = atan(imag(y),real(y))
  // Divide : (x/y)
  r = (xr/yr)*exp((xa-ya)*%i)
endfunction

