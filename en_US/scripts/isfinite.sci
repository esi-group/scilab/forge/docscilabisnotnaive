// Copyright (C) 2010 - Michael Baudin

function TF = isfinite(A)
  // Array elements that are finite
  // http://www.mathworks.fr/help/techdoc/ref/isfinite.html
  TF = (abs(A)<%inf)
endfunction

if ( %f ) then
  ieee(2)
  a = [-2  -1  0  1  2];
  isfinite(1 ./ a) // T T F T T
end

