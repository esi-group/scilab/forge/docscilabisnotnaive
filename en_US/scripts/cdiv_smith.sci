// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// smith --
//   Returns the complex division r = x / y  by Smith's method
//
function r = cdiv_smith ( x,y )
  a = real(x)
  b = imag(x)
  c = real(y)
  d = imag(y)
  if ( abs(d) <= abs(c) ) then
    r = d/c
    den = c + d * r
    e = (a + b * r) / den
    f = (b - a * r) / den
  else
    r = c/d
    den = c * r + d
    e = (a * r + b) / den
    f = (b * r - a) / den
  end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
  r = complex(e,f)
endfunction

