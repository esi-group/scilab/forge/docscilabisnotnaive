// Copyright (C) 2009 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

exec("cdiv_naive.sci");
exec("cdiv_smith.sci");
exec("cdiv_smith2.sci");

ieee(2);
// An easy case.
r = cdiv_naive ( 1.0 + %i * 2.0 , 3.0 + %i * 4.0 )
(1.0 + 2.0 * %i)/(3.0 + 4.0 * %i)
// A more difficult test case, with overflows
r = cdiv_naive ( 1.0 + %i * 1.0 , 1.0 + %i * 1.e307 )
(1.0 + %i * 1.0)/(1.0 + %i * 1.e307)
// A more difficult test case, with underflows
r = cdiv_naive ( 1.0 + %i * 1.0 , 1.e-307 + %i * 1.e-307 )
(1.0 + %i * 1.0)/(1.e-307 + %i * 1.e-307)

//
// compare --
//   Compare the naive complex division and Scilab's division operator
// a,b,c,d : real numbers of the division (a + ib)/(c + id)
// e,f : expected real and imaginary part of the division
//
function compare (x, y, z)
  a = real(x)
  b = real(x)
  c = real(y)
  d = real(y)
  e = real(z)
  f = real(z)
  printf("****************\n");
  printf("           %10.5e +  I * %10.5e \n" , a ,  b );
  printf("         / %10.5e +  I * %10.5e \n" , c ,  d );
  printf("         = %10.5e +  I * %10.5e \n" , e ,  f );
  // Use naive computation
  r = cdiv_naive ( x, y);
  printf("Naive  --> %10.5e +  I * %10.5e\n" , real(r) ,  imag(r) );
  complexerrors ( r , z );
  // Use Scilab's division operator.
  div = x/y
  printf("Scilab --> %10.5e +  I * %10.5e\n" , real(div) ,  imag(div) );
  complexerrors ( div , z );
endfunction
// Compute various errors: in module,
//   w.r.t. complex magnitude, w.r.t. to real part, w.r.t. to imaginary part
// x : computed complex
// y : expected complex
function complexerrors ( x , y )
  err1 = myerror ( x , y );
  err2 = myerror ( real(x) , real(y) );
  err3 = myerror ( imag(x) , imag(y) );
  printf("   e1 = %10.5e, e2 = %10.5e, e3 = %10.5e\n", err1, err2, err3);
endfunction
// Compute a relative error when possible, and an absolute error if not.
// x : computed complex
// y : expected complex
function err = myerror ( x , y )
  if ( y == 0 ) then
    err = abs(x - y);
  else
    err = abs(x - y)/abs(y);
  end
endfunction

ieee(2);
// Check that naive implementation does not have a bug
compare (1+%i*2,3+%i*4, 11/25+%i* 2/25);

// Check that naive implementation is not robust with respect to overflow
compare (1+%i* 1, 1+%i* 1e307, 1e-307+%i* -1e-307);

// Check that naive implementation is not robust with respect to underflow
compare (1+%i* 1, 1e-307+%i* 1e-307, 1e307+%i* 0.);

//
// rootsdemo.sf --
//   A root-finder algorithm based on the eigenvalues of the 
//   companion matrix of the polynomial.
//
function result = rootsdemo(p)
  A=companion(p);
  result = spec(A)
endfunction


// An easy case.
r = cdiv_smith ( 1.0 + %i * 2.0 , 3.0 + %i * 4.0 )
(1.0 + %i * 2.0 )/(3.0 + %i * 4.0)
// A more difficult test case, with overflows
r = cdiv_smith ( 1.0 + %i * 1.0 , 1.0 + %i * 1.e307 )
(1.0 + %i * 1.0)/(1.0 + %i * 1.e307)
// A more difficult test case, with underflows
r = cdiv_smith ( 1.0 + %i * 1.0 , 1.e-307 + %i * 1.e-307 )
(1.0 + %i * 1.0)/(1.e-307 + %i * 1.e-307)

// A difficult case for Smith
r = cdiv_naive ( 1.e307 + %i * 1.e-307 , 1.e204 + %i * 1.e-204 )
r = cdiv_smith ( 1.e307 + %i * 1.e-307 , 1.e204 + %i * 1.e-204 )
(1.e307 + %i * 1.e-307)/(1.e204 + %i * 1.e-204)

//////////////////////////////////////////////////////
// Test smith2, with ideas from Stewart

// An easy case.
r = cdiv_smith2 ( 1.0 + %i * 2.0 , 3.0 + %i * 4.0 )
(1.0 + %i * 2.0)/(3.0 + %i * 4.0)
// A more difficult test case, with overflows
r = cdiv_smith2 ( 1.0 + %i * 1.0 , 1.0 + %i * 1.e307 )
(1.0 + %i * 1.0)/(1.0 + %i * 1.e307)
// A more difficult test case, with underflows
r = cdiv_smith2 ( 1.0 + %i * 1.0 , 1.e-308 + %i * 1.e-308 ) // Wrong !!! Fix it !
(1.0 + %i * 1.0)/(1.e-308 + %i * 1.e-308) 

// A difficult case for Smith
r = cdiv_smith2 ( 1.e307 + %i * 1.e-307 , 1.e204 + %i * 1.e-204 )
(1.e307 + %i * 1.e-307)/(1.e204 + %i * 1.e-204)

//////////////////////////////////////////////////////
// Find a counter-example for Smith's algorithm

// Print all m,n so that the following complex division is exact 
// with doubles floating point numbers.
// (10^n + i 10^-n)/(10^m + i 10^-m) = 10^(n-m) + i 10^(n-3m)
// There are 22484 such couples.
N = 0
for m = 5:308
  for n = 13:308
    if ( (m + 8 < n) & (n-m>=0) & (n-m <308) & (n-3*m >= -307) & (n-3*m<=0) ) then
      N = N + 1;
      //mprintf ( "------------\n")
      //mprintf ( "n=%d, m=%d\n", n, m)
      //mprintf ( "n-m=%d, n-3m=%d\n", n-m, n-3*m)
      //pause
     end
   end
end

// Print all m,n so that the following complex division is exact 
// with doubles floating point numbers.
// (10^n + i 10^-n)/(10^m + i 10^-m) = 10^(n-m) + i 10^(n-3m)
// and n and m are multiples of 20
// There are 59 such couples.
N = 0;
for m = 5:308
  for n = 13:308
    if ( (m + 8 < n) & (n-m>=0) & (n-m <308) & (n-3*m >= -307) & (n-3*m<=0) & modulo(n,20)==0 & modulo(m,20)==0 ) then
      N = N + 1;
      mprintf ( "n=%d, m=%d, n-m=%d, n-3m=%d\n", n, m,n-m, n-3*m)
     end
   end
end

// Print all m,n so that the following complex division is exact 
// with doubles floating point numbers.
// (10^n + i 10^-n)/(10^m + i 10^-m) = 10^(n-m) + i 10^(n-3m)
// and Smith's method fails.
// There are 2752 such couples.
N = 0
for m = 163:308
  for n = 13:308
    if ( (m + 8 < n) & (n-m>=0) & (n-m <308) & (n-3*m >= -307) & (n-3*m<=0) & (n+m>324) ) then
      N = N + 1;
      verbose = %t;
      if ( verbose & modulo(N,10) == 0 ) then
        mprintf ( "N=%d, n=%d, m=%d,n-m=%d, n-3m=%dn n+m =%d\n", N , n, m,n-m, n-3*m , n+m)
      end
    end
  end
end

// Print all m,n so that the following complex division is exact 
// with doubles floating point numbers.
// (10^n + i 10^-n)/(10^m + i 10^-m) = 10^(n-m) + i 10^(n-3m)
// and Smith's method fails.
// and n,m are multiples of 20.
// There are 5 such couples.
N = 0
for m = 163:308
  for n = 13:308
    if ( (m + 8 < n) & (n-m>=0) & (n-m <308) & (n-3*m >= -307) & (n-3*m<=0) & (n+m>324) & modulo(n,10)==0 & modulo(m,10)==0 ) then
      N = N + 1;
      verbose = %t;
      if ( verbose ) then
        mprintf ( "N=%d, n=%d, m=%d, n-m=%d, n-3m=%dn n+m =%d\n", N, n, m,n-m, n-3*m , n+m)
      end
    end
  end
end

n = [210,220,230,240,250,260,270,280,290,300,240,250,260,270,280,290,300,270,280,290,300,300]
m = [170,170,170,170,170,170,170,170,170,170,180,180,180,180,180,180,180,190,190,190,190,200]

