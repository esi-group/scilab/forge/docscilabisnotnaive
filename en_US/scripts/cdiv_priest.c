// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// References
//
// "Efficient scaling for complex division"
// Douglas M. Priest
// Sun Microsystems
// ACM Transactions on Mathematical software, Vol. 30, No. 4, December 2004


/*
* Set v[0] + i v[1] := (z[0] + i z[1]) / (w[0] + i w[1]) assuming:
*
* 1. z[0], z[1], w[0], w[1] are all finite, and w[0], w[1] are not
* both zero
*
* 2. "int" refers to a 32-bit integer type, "long long int" refers
* to a 64-bit integer type, and "double" refers to an IEEE 754-
* compliant 64-bit floating point type
*/
void cdiv(double *v, const double *z, const double *w)
{
    union {
        long long int i;
        double d;
    } aa, bb, cc, dd, ss;
    double a, b, c, d, t;
    int ha, hb, hc, hd, hz, hw, hs;
    /* name the components of z and w */
    a = z[0];
    b = z[1];
    c = w[0];
    d = w[1];
    /* extract high-order 32 bits to estimate |z| and |w| */
    aa.d = a;
    bb.d = b;
    ha = (aa.i >> 32) & 0x7fffffff;
    hb = (bb.i >> 32) & 0x7fffffff;
    hz = (ha > hb)? ha : hb;
    cc.d = c;
    dd.d = d;
    hc = (cc.i >> 32) & 0x7fffffff;
    hd = (dd.i >> 32) & 0x7fffffff;
    hw = (hc > hd)? hc : hd;
    /* compute the scale factor */
    if (hz < 0x07200000 && hw >= 0x32800000 && hw < 0x47100000) {
        /* |z| < 2^-909 and 2^-215 <= |w| < 2^114 */
        hs = (((0x47100000 - hw) >> 1) & 0xfff00000) + 0x3ff00000;
    } 
    else
    {
        hs = (((hw >> 2) - hw) + 0x6fd7ffff) & 0xfff00000;
    }
    ss.i = (long long int) hs << 32;
    /* scale c and d, and compute the quotient */
    c *= ss.d;
    d *= ss.d;
    t = 1.0 / (c * c + d * d);
    c *= ss.d;
    d *= ss.d;
    v[0] = (a * c + b * d) * t;
    v[1] = (b * c - a * d) * t;
}

