// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// An improved Smith method, with an exp-log product
function r = cdiv_smith3 (x,y)
  a = real(x)
  b = imag(x)
  c = real(y)
  d = imag(y)
  if ( abs(d) <= abs(c) ) then
    z = 1 / c
    den = c + d * (d * z)
    e = (a + prodexplog ( [ b d z ] ) ) / den
    f = (b - prodexplog ( [ a d z ] ) ) / den
  else
    z = 1 / d
    den = c * (c * z) + d
    e = ( prodexplog ( [ a c z ] ) + b) / den
    f = ( prodexplog ( [ b c z ] ) - a) / den
  end
  // Avoid using x + %i * y, which may create
  // %inf+%i*%inf, which actually multiplies 0 and %inf,
  // generating an unwanted %nan.
  r = complex(e,f)
endfunction

// A product of a vector.
// Based on log(exp) trick : cost much more !
function p = prodexplog ( v )
  pe = exp(sum(log(v)))
  p = abs(pe) * sign(real(pe))
endfunction

if ( %f ) then
p = prodexplog ( [1 3 1 2] ) // 6
p = prodexplog ( [-1 2 -3 4 1 -2 3 4] ) //  -576
p = prodexplog ( [-1 1 1] )   // -1
p = prodexplog ( [1 -1 1] )   // -1
p = prodexplog ( [1.e200 1.e200 1.e-100] ) //  1.+300

end

