// Copyright (C) 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// A Scilab port of Stewart's algorithm.
// References
//
// "A Note on Complex Division"
// G. W. Stewart, University of Maryland
// ACM Transactions on Mathematical Software, Vol. 11
// No 3, September 1985, Pages 238-241
// 
// G. W. Stewart
// Corrigendum: ``{A} Note on Complex Division''",
// TOMS, Volume 12, Number 3, Pages = 285--285, September, 1986

function r = cdiv_smithStewart ( x,y )
    c = real(x)
    d = imag(x)
    e = real(y)
    f = imag(y)
    flip = %f
    if ( abs(f) >= abs(e) ) then
      [f,e]=switch(e,f)
      [c,d]=switch(d,c)
      flip=%t
    end
    s=1/e
    t=1/(e+f*(f*s))
    if ( abs(f) >= abs(s) ) then
      [f,s]=switch(s,f)
    end
    if ( abs(d) >= abs(s) ) then
      a=t*(c+s*(d*f))
    elseif ( abs(d) >= abs(f) ) then
      a=t*(c+d*(s*f))
    else
      a=t*(c+f*(s*d))
    end
    if ( abs(c) >= abs(s) ) then
      b=t*(d-s*(c*f))
    elseif ( abs(c) >= abs(f) ) then
      b=t*(d-c*(s*f))
    else
      b=t*(d-f*(s*c))
    end
    if ( flip ) then
      b=-b
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    r = complex(a,b)
endfunction

function [c,d]=switch(a,b)
  c=a
  d=b
endfunction

function r = cdiv_smithStewart_Other ( x,y )
    c = real(x)
    d = imag(x)
    e = real(y)
    f = imag(y)
    flip = %f
    if ( abs(f) >= abs(e) ) then
      // Switch e and f
      tmp=e
      e=f
      f=tmp
      // Switch c and d
      tmp=c
      c=d
      d=tmp
      flip=%t
    end
    s=1/e
    t=1/(e+f*(f*s))
    if ( abs(f) >= abs(s) ) then
      // Switch f and s
      tmp=f
      f=s
      s=tmp
    end
    if ( abs(d) >= abs(s) ) then
      a=t*(c+s*(d*f))
    elseif ( abs(d) >= abs(f) ) then
      a=t*(c+d*(s*f))
    else
      a=t*(c+f*(s*d))
    end
    if ( abs(c) >= abs(s) ) then
      b=t*(d+s*(c*f))
    elseif ( abs(c) >= abs(f) ) then
      b=t*(d+c*(s*f))
    else
      b=t*(d+f*(s*c))
    end
    if ( flip ) then
      b=-b
    end
    // Avoid using x + %i * y, which may create
    // %inf+%i*%inf, which actually multiplies 0 and %inf,
    // generating an unwanted %nan.
    r = complex(a,b)
endfunction

