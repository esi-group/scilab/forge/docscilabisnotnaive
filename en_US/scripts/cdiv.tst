// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



// Some attempts to improve Smith's algorithm

haveimproved = %t;

// cdiv_naive.sci : a naive method
exec("cdiv_naive.sci");

// cdiv_smith.sci : original Smith's method
exec("cdiv_smith.sci");

// cdiv_smithStewart.sci : original Smith's method, improved by Steward
exec("cdiv_smithStewart.sci");

// A Scilab port of the ANSI/ISO C algorithm
exec("cdiv_ansiisoC.sci");

// A Scilab port of the Li et al. improved Smith
exec("cdiv_smithLi.sci");

// A division based on polar form
exec("cdiv_polar.sci");

// An improved Smith method, with ideas from Stewart (prodminmax)
exec("cdiv_smith2.sci");

// An improved Smith method, with an exp-log product
exec("cdiv_smith3.sci");

if ( haveimproved ) then
  // An improved Smith method
  exec("cdiv_smith4.sci");
  // An improved Smith, with ideas from Li et al.
  exec("cdiv_smith5.sci");
  // An improved Smith, with ideas from Li et al.
  exec("cdiv_smith6.sci");
end



exec("assert_computedigits.sci");
exec("assert_datasetread.sci");



ieee(2);

function [msg,digits] = fmtdig ( x , y , expected , divfunc , headermsg , footermsg )
  // Performs the complex division x/y, compare to expected, and produve a message 
  // containing the number of digits in the real and imaginary parts.
  r = divfunc ( a + %i * b , c + %i * d );
  digits = assert_computedigits(r,expectedR,basis);
  msg = msprintf("%s=%2d%s\n", headermsg , digits , footermsg )
endfunction

function r = cdiv_scilab ( x , y )
  // Performs the complex division with Scilab /
  r = x/y;
endfunction


////////////////////////////////////////////////////////////////////////
// 
// Check accuracy
format("e",10);
path=pwd();
basis = 2;
msg=[];
dg=[];
dataset = fullfile(path,"cdiv.dataset.csv");
table = assert_datasetread ( dataset , "#" , "," , %t );
ntests = size(table,"r");
atable = evstr(table(:,1));
btable = evstr(table(:,2));
ctable = evstr(table(:,3));
dtable = evstr(table(:,4));
etable = evstr(table(:,5));
ftable = evstr(table(:,6));
for k = 1 : ntests
  a = atable(k);
  b = btable(k);
  c = ctable(k);
  d = dtable(k);
  e = etable(k);
  f = ftable(k);
  //
  expectedR = e+%i*f;
  x = a + %i * b;
  y = c + %i * d;
  msg(1) = msprintf("Test #%3d/%d, %70s, ",  k,ntests,sci2exp([a b c d e f]) );
  dg(k,1)=0;
  j=2;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_scilab , "Sc" , ", " );
  j=j+1;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_naive , "Na" , ", " );
  j=j+1;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith , "Sm" , ", " );
  j=j+1;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smithStewart , "St" , ", " );
  j=j+1;
  //[msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith2 , "S2" , ", " );
  //j=j+1;
  //[msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith3 , "S3" , ", " );
  //j=j+1;
  if ( haveimproved ) then
    [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith4 , "S4" , ", " );
  j=j+1;
  end
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_ansiisoC , "AC", ", "  );
  j=j+1;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smithLi , "Li" , ", " );
  j=j+1;
  [msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith5 , "S5" , "" );
  j=j+1;
  //[msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_smith6 , "S6" , ", " );
  //j=j+1;
  //[msg(j),dg(k,j)] = fmtdig ( x , y , expectedR , cdiv_polar , "PO" , "" );
  //j=j+1;
  mprintf("%s\n",strcat(msg,""));
end
mprintf("Average:\n");
j=2;
mprintf("Sc=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Na=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Sm=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("ST=%d\n",mean(dg(:,j)));
j=j+1;
//mprintf("S2=%d\n",mean(dg(:,j)));
//j=j+1;
//mprintf("S3=%d\n",mean(dg(:,j)));
//j=j+1;
mprintf("S4=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("AC=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("Li=%d\n",mean(dg(:,j)));
j=j+1;
mprintf("S5=%d\n",mean(dg(:,j)));
j=j+1;
//mprintf("S6=%d\n",mean(dg(:,j)));
//j=j+1;
//mprintf("PO=%d\n",mean(dg(:,j)));
//j=j+1;


// Discovered Issues:
// (0+%i*1 )/(0+%i*0) Scilab = Nan + Nan i     Octave = NaN + Inf i   Matlab = NaN + Inf i
// (1+%i*-1)/(0+%i*0) Scilab = Nan + Nan i     Octave = Inf - Inf i   Matlab = Inf - Inf i
// (1+%i*1)/(0+%i*0)  Scilab = NaN + NaN i     Octave = Inf + Inf i   Matlab = Inf + Inf i
//     cdiv_ansiisoC(1+%i*1,0+%i*0) = NaN + Infinity i 
// This is because %inf + %inf * %i produces NaN + Infinity i. Is this right ?

// (%inf+%i*1)/(1+%i*1)  Scilab = NaN - Infinity i  Octave = Inf - Inf i, Matlab = Inf - Inf i 
//    but smith, ansiisoc = NaN - Infinity i  
//
// smith3 - improved Smith method, with an exp-log product - does not improve much.
// Li improves for some case, but not all.
// Only smith5 works in all the current test cases.
//

