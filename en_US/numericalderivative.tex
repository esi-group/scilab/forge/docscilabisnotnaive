% Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin
%
% This file must be used under the terms of the 
% Creative Commons Attribution-ShareAlike 3.0 Unported License :
% http://creativecommons.org/licenses/by-sa/3.0/

\section{Numerical derivatives}

In this section, we analyze the computation of the numerical derivative of 
a given function.

In the first part, we briefly report the first order forward formula, which 
is based on the Taylor theorem.
We then present the naive algorithm based on these mathematical formulas. 
In the second part, we make some experiments in Scilab and compare our
naive algorithm with the \scifun{derivative} Scilab function.
In the third part, we analyze why and how floating point numbers must be taken 
into account when we compute numerical derivatives.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Theory}

The basic result is the Taylor formula with one variable \cite{dixmier}.
Assume that $x\in\RR$ is a given number  and $h\in\RR$ is a given step. Assume 
that $f:\RR\rightarrow\RR$ is a two times continuously differentiable function. 
Therefore,
\begin{eqnarray}
f(x+h) &=& f(x) 
+ h f^\prime(x)
+\frac{h^2}{2} f^{\prime \prime}(x) + \mathcal{O}(h^3).
\end{eqnarray}
We immediately get the forward difference which approximates the first derivate at order 1 
\begin{eqnarray}
f^\prime(x) &=& \frac{f(x+h)  - f(x)}{h} + \frac{h}{2} f^{\prime \prime}(x) + \mathcal{O}(h^2).
\end{eqnarray}

The naive algorithm to compute the numerical derivate of 
a function of one variable is presented in figure \ref{naive-numericalderivative}.

\begin{algorithm}[htbp]
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
\Input{$x,h$}
\Output{$f'(x)$}
$f'(x) := (f(x+h)-f(x))/h$\;
\caption{Naive algorithm to compute the numerical derivative of a function of one variable.}
\label{naive-numericalderivative}
\end{algorithm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Experiments}

The following Scilab function \scifun{myfprime} is a straightforward implementation
of the previous algorithm.
\lstset{language=scilabscript}
\begin{lstlisting}
function fp = myfprime(f,x,h)
  fp = (f(x+h) - f(x))/h;
endfunction
\end{lstlisting}

In our experiments, we will compute the derivatives of the 
square function $f(x)=x^2$, which is $f'(x)=2x$.
The following Scilab function \scifun{myfunction} computes the square function.
\lstset{language=scilabscript}
\begin{lstlisting}
function y = myfunction (x)
  y = x*x;
endfunction
\end{lstlisting}

The (naive) idea is that the computed relative error 
is small when the step $h$ is small. Because \emph{small}
is not a priori clear, we take $h= 10^{-16}$
as a "good" candidate for a \emph{small} double.

The \scifun{derivative} function allows to compute the Jacobian and 
the Hessian matrix of a given function.
Moreover, we can use formulas of order 1, 2 or 4. 
The \scifun{derivative} function has been designed by Rainer von Seggern 
and Bruno Pin{\c c}on. The order 1 formula is the forward numerical 
derivative that we have already presented.

\index{\scifun{derivative}}
In the following script, we compare the computed 
relative error produced by our naive method with step
$h=10^{-16}$ and the \scifun{derivative} function with
default optimal step. We compare the two methods for the point $x=1$.
\lstset{language=scilabscript}
\begin{lstlisting}
x = 1.0;
fpref = derivative(myfunction,x,order=1);
e = abs(fpref-2.0)/2.0;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.e-16;
fp = myfprime(myfunction,x,h);
e = abs(fp-2.0)/2.0;
mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);
\end{lstlisting}

The previous script produces the following output.
\begin{lstlisting}
Scilab f'=2.000000e+000, error=7.450581e-009
Naive f'=0.000000e+000, h=1.000000e-016, error=1.000000e+000
\end{lstlisting}

Our naive method seems to be inaccurate and has no 
significant decimal digit. The Scilab function, instead, 
has 9 significant digits.

Since our faith is based on the truth of the mathematical
theory, some deeper experiments must be performed.
We make the following numerical experiment: we take 
the initial step $h=1.0$ and divide $h$ by 10 at each
step of a loop made of 20 iterations.
\lstset{language=scilabscript}
\begin{lstlisting}
x = 1.0;
fpref = derivative(myfunction,x,order=1);
e = abs(fpref-2.0)/2.0;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.0;
for i=1:20
  h=h/10.0;
  fp = myfprime(myfunction,x,h);
  e = abs(fp-2.0)/2.0;
  mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);
end
\end{lstlisting}

The previous script produces the following output.
\begin{lstlisting}
Scilab f'=2.000000e+000, error=7.450581e-009
Naive f'=2.100000e+000, h=1.000000e-001, error=5.000000e-002
Naive f'=2.010000e+000, h=1.000000e-002, error=5.000000e-003
Naive f'=2.001000e+000, h=1.000000e-003, error=5.000000e-004
Naive f'=2.000100e+000, h=1.000000e-004, error=5.000000e-005
Naive f'=2.000010e+000, h=1.000000e-005, error=5.000007e-006
Naive f'=2.000001e+000, h=1.000000e-006, error=4.999622e-007
Naive f'=2.000000e+000, h=1.000000e-007, error=5.054390e-008
Naive f'=2.000000e+000, h=1.000000e-008, error=6.077471e-009
Naive f'=2.000000e+000, h=1.000000e-009, error=8.274037e-008
Naive f'=2.000000e+000, h=1.000000e-010, error=8.274037e-008
Naive f'=2.000000e+000, h=1.000000e-011, error=8.274037e-008
Naive f'=2.000178e+000, h=1.000000e-012, error=8.890058e-005
Naive f'=1.998401e+000, h=1.000000e-013, error=7.992778e-004
Naive f'=1.998401e+000, h=1.000000e-014, error=7.992778e-004
Naive f'=2.220446e+000, h=1.000000e-015, error=1.102230e-001
Naive f'=0.000000e+000, h=1.000000e-016, error=1.000000e+000
Naive f'=0.000000e+000, h=1.000000e-017, error=1.000000e+000
Naive f'=0.000000e+000, h=1.000000e-018, error=1.000000e+000
Naive f'=0.000000e+000, h=1.000000e-019, error=1.000000e+000
Naive f'=0.000000e+000, h=1.000000e-020, error=1.000000e+000
\end{lstlisting}

We see that the relative error begins by decreasing, gets to a minimum 
and then increases. Obviously, the optimum step is approximately $h=10^{-8}$, where the
relative error is approximately $e_r=6.10^{-9}$. 
We should not be surprised to see that Scilab has computed 
a derivative which is near the optimum.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Explanations}

In this section, we make reasonable assumptions for the 
expression of the total error and compute the optimal step of a forward
difference formula. We extend our work to the centered two 
points formula.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Floating point implementation}

The first source of error is obviously the truncation error 
$E_t(h) = h |f^{\prime \prime}(x)|/2$, due to the limited Taylor expansion.

The other source of error is generated by the roundoff errors in the function 
evaluation of the formula $(f(x+h) - f(x))/h$. 
Indeed, the floating point representation of the function value at point $x$ is 
\begin{eqnarray}
fl(f(x)) = (1+e(x))f(x),
\end{eqnarray}
where the relative error $e$ depends on the the current point $x$.
We assume here that the relative error $e$ is bounded by the product of a constant $c>0$
and the machine precision $r$. Furthermore, we assume here that the constant 
$c$ is equal to one.
We may consider other rounding errors sources, such as the error in the 
sum $x+h$, the difference $f(x+h)-f(x)$ or the division $(f(x+h)-f(x))/h$.
But all these rounding errors can be neglected for they are 
not, in general, as large as the roundoff error generated by the function 
evaluation.
Hence, the roundoff error associated with the function evaluation is $E_r(h)=r|f(x)|/h$.

Therefore, the total error associated with the forward finite difference is bounded by 
\begin{eqnarray}
\label{eq-scilabnaive-totalerror}
E(h) = \frac{r|f(x)|}{h} + \frac{h}{2} |f^{\prime \prime}(x)|.
\end{eqnarray}

The error is then the sum of a
term which is a decreasing function of $h$ and a term which an increasing function of $h$.
We consider the problem of finding the step $h$ which minimizes the error $E(h)$.
The total error $E(h)$ is minimized when its first derivative is zero.
The first derivative of the function $E$ is 
\begin{eqnarray}
E^\prime(h) = -\frac{r|f(x)|}{h^2} + \frac{1}{2} |f^{\prime \prime}(x)|.
\end{eqnarray}
The second derivative of $E$ is 
\begin{eqnarray}
E^{\prime\prime}(h) = 2\frac{r|f(x)|}{h^3}.
\end{eqnarray}
If we assume that $f(x)\neq 0$, then the second derivative $E^{\prime\prime}(h)$ is 
strictly positive, since $h>0$ (i.e. we consider only non-zero steps).
Hence, there is only one global solution of the minimization problem.
This first derivative is zero if and only if 
\begin{eqnarray}
-\frac{r|f(x)|}{h^2} + \frac{1}{2} |f^{\prime \prime}(x)| = 0
\end{eqnarray}
Therefore, the optimal step is 
\begin{eqnarray}
\overline{h} = \sqrt{\frac{2r|f(x)|}{|f^{\prime \prime}(x)|}}.
\end{eqnarray}
Let us make the additional assumption 
\begin{eqnarray}
\frac{2|f(x)|}{|f^{\prime \prime}(x)|} \approx 1.
\end{eqnarray}
Then the optimal step is 
\begin{eqnarray}
\overline{h} = \sqrt{r},
\end{eqnarray}
where the error is 
\begin{eqnarray}
E(\overline{h}) = 2 \sqrt{r}.
\end{eqnarray}

With double precision floating point numbers, we have $r=10^{-16}$ and 
we get $\overline{h} = 10^{-8}$ and $E(\overline{h})=2. 10^{-8}$.
Under our assumptions on $f$ and on the form of the total error, this is the 
minimum error which is achievable with a forward difference
numerical derivate.

We can extend the previous method to the first derivate computed by a centered 2 points 
formula. We can prove that 
\begin{eqnarray}
f^\prime(x) &=& \frac{f(x+h) - f(x-h)}{2h} + \frac{h^2}{6} f^{\prime \prime \prime}(x) + \mathcal{O}(h^3).
\end{eqnarray}
We can apply the same method as previously and, under reasonable assumptions
on $f$ and the form of the total error, we get that the 
optimal step is $h = r^{1/3}$, which corresponds to the total error $E=2r^{2/3}$.
With double precision floating point numbers, this corresponds to 
$h \approx 10^{-5}$ and $E\approx 10^{-10}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Robust algorithm}

A more robust algorithm to compute the numerical derivate of 
a function of one variable is presented in figure \ref{robust-numericalderivative}.

\begin{algorithm}[htbp]
$h := \sqrt{r}$\;
$f'(x) := (f(x+h)-f(x))/h$\;
\caption{A more robust algorithm to compute the numerical derivative of a function of one variable.}
\label{robust-numericalderivative}
\end{algorithm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{One more step}

In this section, we analyze the behavior of the \scifun{derivative} function 
when the point $x$ is either large in magnitude, 
small or close to zero. 
We compare these results with the \scifun{numdiff} function,
which does not use the same step strategy. As we are going 
to see, both functions performs the same when $x$ is near 1, but 
performs very differently when x is large or small.

The \scifun{derivative} function uses the optimal step 
based on the theory we have presented. But the optimal step does not 
solve all the problems that may occur in practice, as we are going to 
see. 

See for example the following Scilab session, where we compute the 
numerical derivative of $f(x)=x^2$ for $x=10^{-100}$. The 
expected result is $f'(x) = 2. \times 10^{-100}$.
\begin{lstlisting}
-->fp = derivative(myfunction,1.e-100,order=1)
 fp  =
    0.0000000149011611938477  
-->fe=2.e-100
 fe  =
    2.000000000000000040-100  
-->e = abs(fp-fe)/fe
 e  =
    7.450580596923828243D+91  
\end{lstlisting}

The result does not have any significant digit.

The explanation is that the step is $h = \sqrt{r}\approx 10^{-8}$.
Then, the point $x+h$ is computed as $10^{-100} + 10^{-8}$ which is 
represented by a floating point number which is close to $10^{-8}$, because the 
term $10^{-100}$ is much smaller than $10^{-8}$. 
Then we evaluate the function, which leads to $f(x+h)=f(10^{-8}) = 10^{-16}$. The result of the 
computation is therefore $(f(x+h) - f(x))/h = (10^{-16} + 10^{-200})/10^{-8} \approx 10^{-8}$.

That experiment shows that the \scifun{derivative} function uses a 
poor default step $h$ when $x$ is very small.

To improve the accuracy of the computation, we can take the control of the 
step $h$. A reasonable solution is to use $h=\sqrt{r}|x|$ so that the 
step is scaled depending on $x$. 
The following script illustrates than method, which produces 
results with 8 significant digits.
\begin{lstlisting}
-->fp = derivative(myfunction,1.e-100,order=1,h=sqrt(%eps)*1.e-100)
 fp  =
    2.000000013099139394-100  
-->fe=2.e-100
 fe  =
    2.000000000000000040-100  
-->e = abs(fp-fe)/fe
 e  =
    0.0000000065495696770794  
\end{lstlisting}

But when $x$ is exactly zero, the step $h=\sqrt{r}|x|$ cannot work, because 
it would produce the step $h=0$, which would generate a division by zero
exception. In that case, the step $h=\sqrt{r}$ provides a sufficiently 
good accuracy.

Another function is available in Scilab to compute the 
numerical derivatives of a given function, that is \scifun{numdiff}.
The \scifun{numdiff} function uses the step 
\begin{eqnarray}
h=\sqrt{r}(1+10^{-3}|x|).
\end{eqnarray}
In the following paragraphs, we analyze why this formula 
has been chosen. As we are going to check experimentally, this step
formula performs better than \scifun{derivative} when $x$ is 
large, but performs equally bad when $x$ is small.

\index{\scifun{numdiff}}
As we can see the following session, the behavior is approximately 
the same when the value of $x$ is 1.
\begin{lstlisting}
-->fp = numdiff(myfunction,1.0)
 fp  =
    2.0000000189353417390237  
-->fe=2.0
 fe  =
    2.  
-->e = abs(fp-fe)/fe
 e  =
    9.468D-09  
\end{lstlisting}

The accuracy is slightly decreased with respect to the optimal
value 7.450581e-009 which was produced by the \scifun{derivative} function. 
But the number of significant digits is approximately the same, i.e. 9 digits.

The goal of the step used by the \scifun{numdiff} function is to produce good 
accuracy when the value of $x$ is large. In this case, the \scifun{numdiff} function 
produces accurate results, while the \scifun{derivative} function performs poorly.

In the following session, we compute the numerical derivative of the function $f(x)=x^2$
at the point $x=10^{10}$. The expected result is $f'(x)=2.10^{10}$.
\begin{lstlisting}
-->numdiff(myfunction,1.e10)
  ans  =
    2.000D+10  
-->derivative(myfunction,1.e10,order=1)
 ans  =
    0.  
\end{lstlisting}
We see that the \scifun{numdiff} function produces an accurate result while the 
\scifun{derivative} function produces a result which has no significant digit.

The behavior of the two functions when $x$ is close to zero is the same, i.e. both functions 
produce wrong results. Indeed, when we use the \scifun{derivative} function, the 
step $h=\sqrt{r}$ is too large so that the point $x$ is neglected against the step $h$. 
On the other hand, we we use the \scifun{numdiff} function, the step $h=\sqrt{r}(1+10^{-3}|x|)$
is approximated by $h=\sqrt{r}$ so that it produces the same 
results as the \scifun{derivative} function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{References}

A reference for numerical derivatives 
is \cite{AbramowitzStegun1972}, chapter 25. "Numerical Interpolation, 
Differentiation and Integration" (p. 875).
The webpage \cite{schimdtnd} and the book \cite{NumericalRecipes} give
results about the rounding errors.

In order to solve this issue generated by the magnitude of $x$, 
more complex methods should be used.
Moreover, we did not give the solution of other sources of rounding errors.
Indeed, the step $h=\sqrt{r}$ was computed based on assumptions on the 
rounding error of the function evaluations, where we consider that the 
constant $c$ is equal to one. This assumption is satisfied only
in the ideal case. Furthermore, we make the assumption that 
the factor $\frac{2|f(x)|}{|f^{\prime \prime}(x)|}$ is close to one.
This assumption is far from being achieved in practical situations,
where the function value and its second derivative can vary greatly 
in magnitude.

Several authors attempted to solve the problems associated with 
numerical derivatives.
A non-exhaustive list of references includes \cite{KelleyNewtonMethod,DumontetVignes1977,1979SteplemanWinarsky,Gill81MurrayWright}.



